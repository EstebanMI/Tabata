package com.example.esteban.tabata.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by Esteban on 07/11/2017.
 */

public class Programme extends SugarRecord implements Parcelable{
    private String nom;
    private int prepareTime;
    private int workTime;
    private int restTime;
    private int cycleNumber;
    private int tabataNumber;
    private int tabataRestTime;

    /* SUGARORM : OBLIGATOIRE */
    public Programme(){}

    public Programme(String nom, int prepareTime, int workTime, int restTime, int cycleNumber, int tabataNumber, int tabataRestTime){
        setNom(nom);
        setPrepareTime(prepareTime);
        setWorkTime(workTime);
        setRestTime(restTime);
        setCycleNumber(cycleNumber);
        setTabataNumber(tabataNumber);
        setTabataRestTime(tabataRestTime);
    }

    protected Programme(Parcel in) {
        nom = in.readString();
        prepareTime = in.readInt();
        workTime = in.readInt();
        restTime = in.readInt();
        cycleNumber = in.readInt();
        tabataNumber = in.readInt();
        tabataRestTime = in.readInt();
    }

    public static final Creator<Programme> CREATOR = new Creator<Programme>() {
        @Override
        public Programme createFromParcel(Parcel in) {
            return new Programme(in);
        }

        @Override
        public Programme[] newArray(int size) {
            return new Programme[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPrepareTime() {
        return prepareTime;
    }

    public void setPrepareTime(int prepareTime) {
        this.prepareTime = prepareTime;
    }

    public int getWorkTime() {
        return workTime;
    }

    public void setWorkTime(int workTime) {
        this.workTime = workTime;
    }

    public int getRestTime() {
        return restTime;
    }

    public void setRestTime(int restTime) {
        this.restTime = restTime;
    }

    public int getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(int cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public int getTabataNumber() {
        return tabataNumber;
    }

    public void setTabataNumber(int tabataNumber) {
        this.tabataNumber = tabataNumber;
    }

    public int getTabataRestTime() {
        return tabataRestTime;
    }

    public void setTabataRestTime(int tabataRestTime) {
        this.tabataRestTime = tabataRestTime;
    }

    public int getTotalTime(){
        return prepareTime + (workTime*cycleNumber*tabataNumber) +  (restTime*(cycleNumber-1)*tabataNumber) + (tabataRestTime*(tabataNumber-1));
    }

    public String getMinuteSecond(int time){
        long minutes = TimeUnit.SECONDS.toMinutes(time);
        long secondes = TimeUnit.SECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(minutes);

        String textminute = "";
        String textsecond = "";

        if(minutes > 0){
            textminute = String.valueOf(minutes)+"min";
        }

        if (secondes < 10){
            textsecond += "0";
        }
        textsecond += String.valueOf(secondes);

        return textminute+textsecond;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeInt(prepareTime);
        dest.writeInt(workTime);
        dest.writeInt(restTime);
        dest.writeInt(cycleNumber);
        dest.writeInt(tabataNumber);
        dest.writeInt(tabataRestTime);
    }
}
