package com.example.esteban.tabata;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.esteban.tabata.data.Programme;

/**
 * Created by Esteban on 22/11/2017.
 */

public class LaunchProgDialogFragment extends DialogFragment {

    private static final String KEY_PROGRAMME = "programme";

    // Constantes d'envoie
    public final static String TABATA_CONFIG_PREPARE = "prepareTime";
    public final static String TABATA_CONFIG_WORK = "workTime";
    public final static String TABATA_CONFIG_REST = "restTime";
    public final static String TABATA_CONFIG_CYCLE = "cycleNumber";
    public final static String TABATA_CONFIG_TABATA = "tabataNumber";
    public final static String TABATA_CONFIG_TABATA_REST = "tabataRestTime";
    public final static String TABATA_CONFIG_TOTAL = "totalTime";

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static LaunchProgDialogFragment newInstance(Programme prog) {
        LaunchProgDialogFragment f = new LaunchProgDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable(KEY_PROGRAMME, prog);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Programme mProg = (Programme) getArguments().getParcelable(KEY_PROGRAMME);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("Tabata!")
                .setMessage("Lancer le programme  '" + mProg.getNom() + "' ?")
                .setIcon(R.drawable.musclewhite_48x48)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), TabataTimerActivity.class);
                        intent.putExtra(TABATA_CONFIG_PREPARE, mProg.getPrepareTime());
                        intent.putExtra(TABATA_CONFIG_WORK, mProg.getWorkTime());
                        intent.putExtra(TABATA_CONFIG_REST, mProg.getRestTime());
                        intent.putExtra(TABATA_CONFIG_CYCLE, mProg.getCycleNumber());
                        intent.putExtra(TABATA_CONFIG_TABATA, mProg.getTabataNumber());
                        intent.putExtra(TABATA_CONFIG_TABATA_REST, mProg.getTabataRestTime());
                        intent.putExtra(TABATA_CONFIG_TOTAL, mProg.getTotalTime());

                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
