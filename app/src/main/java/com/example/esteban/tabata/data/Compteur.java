package com.example.esteban.tabata.data;

import android.os.CountDownTimer;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ProgressBar;

import java.util.LinkedList;

/**
 * Created by Esteban on 01/12/2017.
 */
public class Compteur extends UpdateSource implements Parcelable{

    // DATA
    private long updatedTime = 0;
    private CountDownTimer timer;   // https://developer.android.com/reference/android/os/CountDownTimer.html
    private Step currentStep;
    private Step nextStep;
    private LinkedList<Step> stepList;
    private int cycleNumber;
    private int tabataNumber;

    private ProgressBar progressBar;

    public Compteur() {}

    public Compteur(LinkedList<Step> list){
        stepList = list;
        if(!stepList.isEmpty()){
            currentStep = stepList.removeFirst();
            if(!stepList.isEmpty()){
                nextStep = stepList.getFirst();
            }
            updatedTime = currentStep.getStepTime();
        }
    }

    ////////// PARCELABLE CONSTRUCTOR //////////

    protected Compteur(Parcel in) {
        updatedTime = in.readLong();
    }

    public static final Creator<Compteur> CREATOR = new Creator<Compteur>() {
        @Override
        public Compteur createFromParcel(Parcel in) {
            return new Compteur(in);
        }

        @Override
        public Compteur[] newArray(int size) {
            return new Compteur[size];
        }
    };

    ///////////////////////////////////////////

    // Lancer le compteur
    public void start() {
        timer = new CountDownTimer(updatedTime, 10) {

            // Callback fired on regular interval
            public void onTick(long millisUntilFinished) {
                updatedTime = millisUntilFinished;
                progressBar.setMax(currentStep.getStepTime());
                progressBar.setProgress((int)updatedTime);
                // Mise à jour
                update();
            }

            // Callback fired when the time is up
            public void onFinish() {
                if(!stepList.isEmpty()){
                    // Récupération de la prochaine étape
                    currentStep = stepList.removeFirst();
                    if(currentStep.getStepName().equals("Work")){
                        cycleNumber--;
                    }
                    if(!stepList.isEmpty()){
                        nextStep =  stepList.getFirst();
                        if(nextStep.getStepName().equals("Repos entre les tabatas")){
                            tabataNumber--;
                        }
                    }else{
                        nextStep = null;
                    }
                    updatedTime = currentStep.getStepTime();
                    Compteur.this.start();
                }else{
                    currentStep = null;
                }

                // Mise à jour
                update();
            }

        }.start();   // Start the countdown
    }

    // Mettre en pause le compteur
    public void pause() {

        if (timer != null) {

            // Arreter le timer
            stop();

            // Mise à jour
            update();
        }
    }

    // Arrete l'objet CountDownTimer et l'efface
    private void stop() {
        timer.cancel();
        timer = null;
    }


    // GETTER & SETTER
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public Step getNextStep() {
        return nextStep;
    }

    public int getTabataNumber() {
        return tabataNumber;
    }

    public void setTabataNumber(int tabataNumber) {
        this.tabataNumber = tabataNumber;
    }

    public int getCycleNumber() {
        return cycleNumber;
    }

    public void setCycleNumber(int cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public Step getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(Step currentStep) {
        this.currentStep = currentStep;
    }

    public LinkedList<Step> getStepList() {
        return stepList;
    }

    public void setStepList(LinkedList<Step> stepList) {
        this.stepList = stepList;
    }

    public int getMinutes() {
        int min = (int) ((updatedTime/1000)/60);
        return min;
    }

    public int getSecondes() {
        int secs = (int) (updatedTime / 1000);
        return secs % 60;
    }

    public int getMillisecondes() {
        return (int) (updatedTime % 1000);
    }

    ////////// PARCELABLE METHODS ///////////

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(updatedTime);
    }

    /////////////////////////////////////////
}
