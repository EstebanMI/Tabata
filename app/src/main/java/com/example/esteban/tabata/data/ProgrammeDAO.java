package com.example.esteban.tabata.data;

import java.util.List;

/**
 * Created by Esteban on 07/11/2017.
 */

public class ProgrammeDAO {

    public static List<Programme> selectAll(){
        return Programme.listAll(Programme.class);
    }

    public static Programme findById(int id){
        return Programme.findById(Programme.class, id);
    }
}
