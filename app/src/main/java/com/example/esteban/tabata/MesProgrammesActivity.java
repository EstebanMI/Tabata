package com.example.esteban.tabata;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.esteban.tabata.data.Programme;
import com.example.esteban.tabata.data.ProgrammeDAO;

import java.util.List;

public class MesProgrammesActivity extends AppCompatActivity implements DeleteDialogFragment.DeleteDialogListener {

    private LinearLayout listeProgrammeView;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_programmes);

        listeProgrammeView = (LinearLayout) findViewById(R.id.listeProgramme);

        miseAJour();
    }

    private void miseAJour() {

        // Récupérer les notes
        List<Programme> listeProgramme = ProgrammeDAO.selectAll();

        // Suppression des éléments dans le linearLayout
        listeProgrammeView.removeAllViews();

        // Afficher les notes
        for (Programme programme : listeProgramme) {
            listeProgrammeView.addView(createProgramme(programme));

            // Création d'un séparateur
            View v = new View(this);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    5
            ));
            v.setBackgroundColor(Color.parseColor("#B3B3B3"));

            listeProgrammeView.addView(v);
        }
    }

    private LinearLayout createProgramme(final Programme programme){

        // Linear
        LinearLayout programLinear = new LinearLayout(this);
        programLinear.setOrientation(LinearLayout.HORIZONTAL);

        // Paramètrages
        LinearLayout.LayoutParams paramText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        paramText.weight = 2;

        LinearLayout.LayoutParams paramTimes = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        paramTimes.weight = 1;

        TextView name = new TextView(this);
        name.setLayoutParams(paramText);
        name.setGravity(Gravity.CENTER_VERTICAL);
        name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24f);
        name.setText(programme.getNom());
        name.setTextColor(getResources().getColor(R.color.colorPrimary));
        name.setPadding(20,5,20,5);

        LinearLayout timesLayout = new LinearLayout(this);
        timesLayout.setOrientation(LinearLayout.VERTICAL);
        timesLayout.setLayoutParams(paramTimes);

        // Nombre de Tabata
        LinearLayout nbTabataLayout = new LinearLayout(this);
        nbTabataLayout.setOrientation(LinearLayout.HORIZONTAL);
        TextView libelleNbTabata = new TextView(this);
        libelleNbTabata.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        libelleNbTabata.setText("Nb Tabata : ");
        TextView nbTabata = new TextView(this);
        nbTabata.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        nbTabata.setText(String.valueOf(programme.getTabataNumber()));

        nbTabataLayout.addView(libelleNbTabata);
        nbTabataLayout.addView(nbTabata);
        timesLayout.addView(nbTabataLayout);

        // Nombre de cycles
        LinearLayout nbCycleLayout = new LinearLayout(this);
        nbCycleLayout.setOrientation(LinearLayout.HORIZONTAL);
        TextView libelleNbCycle = new TextView(this);
        libelleNbCycle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        libelleNbCycle.setText("Nb Cycle : ");
        TextView nbCycle = new TextView(this);
        nbCycle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        nbCycle.setText(String.valueOf(programme.getCycleNumber()));

        nbCycleLayout.addView(libelleNbCycle);
        nbCycleLayout.addView(nbCycle);
        timesLayout.addView(nbCycleLayout);

        // Temps Total
        LinearLayout totalTimeLayout = new LinearLayout(this);
        totalTimeLayout.setOrientation(LinearLayout.HORIZONTAL);
        TextView libelleTotalTime = new TextView(this);
        libelleTotalTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        libelleTotalTime.setText("Temps total : ");
        TextView totalTime = new TextView(this);
        totalTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        totalTime.setText(programme.getMinuteSecond(programme.getTotalTime()));

        totalTimeLayout.addView(libelleTotalTime);
        totalTimeLayout.addView(totalTime);
        timesLayout.addView(totalTimeLayout);

        programLinear.addView(name);
        programLinear.addView(timesLayout);

        programLinear.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDeleteDialog(programme);
                return false;
            }
        });

        programLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchProgDialogFragment newFragment = LaunchProgDialogFragment.newInstance(programme);
                newFragment.show(getFragmentManager(), "launch");
            }
        });

        return programLinear;
    }

    // Méthode appelée quand l'utilisateur clique sur OK pour supprimer un programme
    @Override
    public void onDeleteDialogPositiveClick(DialogFragment dialog, Programme programme) {
        programme.delete();
        miseAJour();
    }

    void showDeleteDialog(Programme programme){
        DeleteDialogFragment newFragment = DeleteDialogFragment.newInstance(programme);
        newFragment.show(getFragmentManager(), "delete");
    }
}
