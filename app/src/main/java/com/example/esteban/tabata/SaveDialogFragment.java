package com.example.esteban.tabata;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.esteban.tabata.data.Programme;

/**
 * Created by Esteban on 24/11/2017.
 */

public class SaveDialogFragment extends DialogFragment {

    // Constantes d'envoie
    public final static String PREPARE_TIME = "prepareTime";
    public final static String WORK_TIME = "workTime";
    public final static String REST_TIME = "restTime";
    public final static String CYCLE_NUMBER = "cycleNumber";
    public final static String TABATA_NUMBER = "tabataNumber";
    public final static String TABATA_REST_TIME = "tabataRestTime";

    public static SaveDialogFragment newInstance(int prepareTime, int workTime, int restTime, int cycleNumber, int tabataNumber, int tabataRestTime) {
        SaveDialogFragment f = new SaveDialogFragment();

        // Création des arguments à passer à l'AlertDialog
        Bundle args = new Bundle();
        args.putInt(PREPARE_TIME, prepareTime);
        args.putInt(WORK_TIME, workTime);
        args.putInt(REST_TIME, restTime);
        args.putInt(CYCLE_NUMBER, cycleNumber);
        args.putInt(TABATA_NUMBER, tabataNumber);
        args.putInt(TABATA_REST_TIME, tabataRestTime);
        f.setArguments(args);

        return f;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Création de l'objet
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);

        // Récupération des arguments
        final int prepareTime = getArguments().getInt(PREPARE_TIME);
        final int workTime = getArguments().getInt(WORK_TIME);
        final int restTime = getArguments().getInt(REST_TIME);
        final int cycleNumber = getArguments().getInt(CYCLE_NUMBER);
        final int tabataNumber = getArguments().getInt(TABATA_NUMBER);
        final int tabataRestTime = getArguments().getInt(TABATA_REST_TIME);

        // Création du EditText placé dans l'AlertDialog
        final EditText input = new EditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
        builder.setView(input);

        builder.setTitle("Enregistrée le programme")
                .setMessage("Entrer un nom")
                .setIcon(android.R.drawable.ic_menu_save)
                .setPositiveButton("Save",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String name = input.getText().toString();
                                //String name = "Text";
                                if(!TextUtils.isEmpty(name)){
                                    // Création du programme et Ajout en BDD
                                    Programme programme = new Programme(name, prepareTime, workTime, restTime, cycleNumber, tabataNumber, tabataRestTime);
                                    programme.save();
                                    Toast.makeText(getActivity(), "Programme Tabata \"" + name + "\" enregistré", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
        // Création de l'AlertDialog et le retourne
        return builder.create();
    }
}
