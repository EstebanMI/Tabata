package com.example.esteban.tabata;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.esteban.tabata.data.Programme;

import java.util.ArrayList;
import java.util.List;

public class TabataConfigurationActivity extends AppCompatActivity{

    // Numéro de requête
    public final static int TABATA_CONFIG_REQUEST = 0;

    // Constantes d'envoie
    public final static String TABATA_CONFIG_PREPARE = "prepareTime";
    public final static String TABATA_CONFIG_WORK = "workTime";
    public final static String TABATA_CONFIG_REST = "restTime";
    public final static String TABATA_CONFIG_CYCLE = "cycleNumber";
    public final static String TABATA_CONFIG_TABATA = "tabataNumber";
    public final static String TABATA_CONFIG_TABATA_REST = "tabataRestTime";
    public final static String TABATA_CONFIG_TOTAL = "totalTime";

    // Constantes (en secondes)
    private final static int PREPARE_TIME_INIT = 10;
    private final static int WORK_TIME_INIT = 20;
    private final static int REST_TIME_INIT = 10;
    private final static int CYCLE_NUMBER_INIT = 8;
    private final static int TABATA_NUMBER_INIT = 1;
    private final static int TABATA_REST_INIT = 0;

    //Constantes MINIMUM (en secondes)
    private final static int PREPARE_TIME_MIN = 0;
    private final static int WORK_TIME_MIN = 1;
    private final static int REST_TIME_MIN = 0;
    private final static int CYCLE_NUMBER_MIN = 1;
    private final static int TABATA_NUMBER_MIN = 1;
    private final static int TABATA_REST_MIN = 0;

    // Data
    private int prepareTime;
    private int workTime;
    private int restTime;
    private int cycleNumber;
    private int tabataNumber;
    private int tabataRestTime;
    private int totalTime;

    // Elements graphiques
    private EditText prepareView;
    private EditText workView;
    private EditText restView;
    private EditText cycleView;
    private EditText tabataView;
    private EditText tabataRestView;


    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabata_configuration);

        // Initialisation des temps
        prepareTime = PREPARE_TIME_INIT;
        workTime = WORK_TIME_INIT;
        restTime = REST_TIME_INIT;
        cycleNumber = CYCLE_NUMBER_INIT;
        tabataNumber = TABATA_NUMBER_INIT;
        tabataRestTime = TABATA_REST_INIT;

        // Récupération des éléments graphiques
        prepareView = (EditText) findViewById(R.id.txt_prepareTimer);
        workView = (EditText) findViewById(R.id.txt_WorkTimer);
        restView = (EditText) findViewById(R.id.txt_restTimer);
        cycleView = (EditText) findViewById(R.id.txt_cycleNumber);
        tabataView = (EditText) findViewById(R.id.txt_tabataNumber);
        tabataRestView = (EditText) findViewById(R.id.txt_tabataRestTimer);

        addListenerOnViewElement();

        // Mise à jour des éléments graphiques
        update();
    }

    // Ajoute des listeners aux images - et + de chaque
    public void addListenerOnViewElement(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ImageView btn_minusPrepare = (ImageView) findViewById(R.id.btn_minusPrepare);
        ImageView btn_plusPrepare = (ImageView) findViewById(R.id.btn_plusPrepare);
        ImageView btn_minusWork = (ImageView) findViewById(R.id.btn_minusWork);
        ImageView btn_plusWork = (ImageView) findViewById(R.id.btn_plusWork);
        ImageView btn_minusRest = (ImageView) findViewById(R.id.btn_minusRest);
        ImageView btn_plusRest = (ImageView) findViewById(R.id.btn_plusRest);
        ImageView btn_minusCycles = (ImageView) findViewById(R.id.btn_minusCycles);
        ImageView btn_plusCycles = (ImageView) findViewById(R.id.btn_plusCycles);
        ImageView btn_minusTabata = (ImageView) findViewById(R.id.btn_minusTabata);
        ImageView btn_plusTabata = (ImageView) findViewById(R.id.btn_plusTabata);
        ImageView btn_minusTabataRest = (ImageView) findViewById(R.id.btn_minusTabataRest);
        ImageView btn_plusTabataRest = (ImageView) findViewById(R.id.btn_plusTabataRest);

        ArrayList<ImageView> imageArrayList = new ArrayList<ImageView>();

        imageArrayList.add(btn_minusPrepare);
        imageArrayList.add(btn_plusPrepare);
        imageArrayList.add(btn_minusWork);
        imageArrayList.add(btn_plusWork);
        imageArrayList.add(btn_minusRest);
        imageArrayList.add(btn_plusRest);
        imageArrayList.add(btn_minusCycles);
        imageArrayList.add(btn_plusCycles);
        imageArrayList.add(btn_minusTabata);
        imageArrayList.add(btn_plusTabata);
        imageArrayList.add(btn_minusTabataRest);
        imageArrayList.add(btn_plusTabataRest);

        for (final ImageView imageView : imageArrayList){
            imageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            onChangeValue(v);
                            mHandler.postDelayed(mAction, 500);
                            break;
                        case MotionEvent.ACTION_UP:
                            mHandler.removeCallbacks(mAction);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            mHandler.removeCallbacks(mAction);
                    }

                    return true;

                }
                Runnable mAction = new Runnable() {
                    @Override
                    public void run() {
                        onChangeValue(imageView);
                        mHandler.postDelayed(this, 50);
                    }
                };
            });
        }

        EditText txt_prepareTimer = (EditText) findViewById(R.id.txt_prepareTimer) ;
        EditText txt_WorkTimer = (EditText) findViewById(R.id.txt_WorkTimer) ;
        EditText txt_restTimer = (EditText) findViewById(R.id.txt_restTimer) ;
        EditText txt_cycleNumber = (EditText) findViewById(R.id.txt_cycleNumber) ;
        EditText txt_tabataNumber = (EditText) findViewById(R.id.txt_tabataNumber) ;
        EditText txt_tabataRestTimer = (EditText) findViewById(R.id.txt_tabataRestTimer) ;

        ArrayList<EditText> editArrayList = new ArrayList<EditText>();

        editArrayList.add(txt_prepareTimer);
        editArrayList.add(txt_WorkTimer);
        editArrayList.add(txt_restTimer);
        editArrayList.add(txt_cycleNumber);
        editArrayList.add(txt_tabataNumber);
        editArrayList.add(txt_tabataRestTimer);

        for (final EditText value : editArrayList){
            value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    switch (value.getId()) {
                        case R.id.txt_prepareTimer:
                            prepareTime = getNumber(s);
                            if(prepareTime == -1){prepareTime = PREPARE_TIME_MIN; update();}
                            break;
                        case R.id.txt_WorkTimer:
                            workTime = getNumber(s);
                            if(workTime == -1){workTime = WORK_TIME_MIN; update();}
                            break;
                        case R.id.txt_restTimer:
                            restTime = getNumber(s);
                            if(restTime == -1){restTime = REST_TIME_MIN; update();}
                            break;
                        case R.id.txt_cycleNumber:
                            cycleNumber = getNumber(s);
                            if(cycleNumber == -1){cycleNumber = CYCLE_NUMBER_MIN; update();}
                            break;
                        case R.id.txt_tabataNumber:
                            tabataNumber = getNumber(s);
                            if(tabataNumber == -1){tabataNumber = TABATA_NUMBER_MIN; update();}
                            break;
                        case R.id.txt_tabataRestTimer:
                            tabataRestTime = getNumber(s);
                            if(tabataRestTime == -1){tabataRestTime = TABATA_REST_MIN; update();}
                            break;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    public void onClickButtonStart(View view){

        // Création de l'intention de l'activité TabataTimerActivity
        Intent intent = new Intent(this, TabataTimerActivity.class);

        intent.putExtra(TABATA_CONFIG_PREPARE, prepareTime);
        intent.putExtra(TABATA_CONFIG_WORK, workTime);
        intent.putExtra(TABATA_CONFIG_REST, restTime);
        intent.putExtra(TABATA_CONFIG_CYCLE, cycleNumber);
        intent.putExtra(TABATA_CONFIG_TABATA, tabataNumber);
        intent.putExtra(TABATA_CONFIG_TABATA_REST, tabataRestTime);
        intent.putExtra(TABATA_CONFIG_TOTAL, totalTime);

        // Lancement de la demande de changement d'acitivité
        startActivityForResult(intent, TABATA_CONFIG_REQUEST);
    }

    public void onClickButtonReset(View view){

        // Réinitialisation des temps
        prepareTime = PREPARE_TIME_INIT;
        workTime = WORK_TIME_INIT;
        restTime = REST_TIME_INIT;
        cycleNumber = CYCLE_NUMBER_INIT;
        tabataNumber = TABATA_NUMBER_INIT;
        tabataRestTime = TABATA_REST_INIT;

        // Mise à jour des éléments graphiques
        update();
    }

    public void onChangeValue(View view){
        switch (view.getId()){
            case R.id.btn_minusPrepare:
                if(prepareTime > PREPARE_TIME_MIN){prepareTime--;};
                break;
            case R.id.btn_plusPrepare:
                prepareTime++;
                break;
            case R.id.btn_minusWork:
                if(workTime > WORK_TIME_MIN){workTime--;}
                break;
            case R.id.btn_plusWork:
                workTime++;
                break;
            case R.id.btn_minusRest:
                if(restTime > REST_TIME_MIN){restTime--;}
                break;
            case R.id.btn_plusRest:
                restTime++;
                break;
            case R.id.btn_minusCycles:
                if(cycleNumber > CYCLE_NUMBER_MIN){cycleNumber--;}
                break;
            case R.id.btn_plusCycles:
                cycleNumber++;
                break;
            case R.id.btn_minusTabata:
                if(tabataNumber > TABATA_NUMBER_MIN){tabataNumber--;}
                break;
            case R.id.btn_plusTabata:
                tabataNumber++;
                break;
            case R.id.btn_minusTabataRest:
                if(tabataRestTime > TABATA_REST_MIN){tabataRestTime--;}
                break;
            case R.id.btn_plusTabataRest:
                tabataRestTime++;
        }

        // Mise à jour des éléments graphiques
        update();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ajoute les entrées de menu_test à l'ActionBar
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                saveTabata();
                break;
            case R.id.action_programmes:
                showProgrammes();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    // Sauvegarde en base de données le Tabata
    public void saveTabata(){
        SaveDialogFragment newFragment = SaveDialogFragment.newInstance(prepareTime, workTime, restTime, cycleNumber, tabataNumber, tabataRestTime);
        newFragment.show(getFragmentManager(), "save");
    }

    // Voir les programmes
    public void showProgrammes(){
        Intent intent = new Intent(TabataConfigurationActivity.this, MesProgrammesActivity.class);

        startActivity(intent);
    }

    // Récupère le nombre saisie dans un EditText
    public int getNumber(CharSequence s){
        try{
            return Integer.parseInt(s.toString());
        }catch (NumberFormatException e){
            Toast.makeText(this, "Valeur saisie incorrect", Toast.LENGTH_SHORT).show();
            return -1;
        }
    }

    // Mise à jour des éléments graphiques
    public void update(){
        setTitle(getString(R.string.total_time) + getTotalTime());
        prepareView.setText(String.valueOf(prepareTime));
        workView.setText(String.valueOf(workTime));
        restView.setText(String.valueOf(restTime));
        cycleView.setText(String.valueOf(cycleNumber));
        tabataView.setText(String.valueOf(tabataNumber));
        tabataRestView.setText(String.valueOf(tabataRestTime));
    }

    // Calcule le temps total du tabata et le convertis en minute/sec
    public String getTotalTime(){
        totalTime = prepareTime + (workTime*cycleNumber*tabataNumber) +  (restTime*(cycleNumber-1)*tabataNumber) + (tabataRestTime*(tabataNumber-1));
        String time = " " + (totalTime/60 >=10 ? String.valueOf(totalTime/60) : "0"+String.valueOf(totalTime/60));
        time = time + ":" + (totalTime%60 >=10 ? String.valueOf(totalTime%60) : "0"+String.valueOf(totalTime%60));
        return time;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putInt(TABATA_CONFIG_PREPARE, prepareTime);
        savedInstanceState.putInt(TABATA_CONFIG_WORK, workTime);
        savedInstanceState.putInt(TABATA_CONFIG_REST, restTime);
        savedInstanceState.putInt(TABATA_CONFIG_CYCLE, cycleNumber);
        savedInstanceState.putInt(TABATA_CONFIG_TABATA, tabataNumber);
        savedInstanceState.putInt(TABATA_CONFIG_TABATA_REST, tabataRestTime);
        savedInstanceState.putInt(TABATA_CONFIG_TOTAL, totalTime);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        prepareTime = savedInstanceState.getInt(TABATA_CONFIG_PREPARE);
        workTime = savedInstanceState.getInt(TABATA_CONFIG_WORK);
        restTime = savedInstanceState.getInt(TABATA_CONFIG_REST);
        cycleNumber = savedInstanceState.getInt(TABATA_CONFIG_CYCLE);
        tabataNumber = savedInstanceState.getInt(TABATA_CONFIG_TABATA);
        tabataRestTime = savedInstanceState.getInt(TABATA_CONFIG_TABATA_REST);
        totalTime = savedInstanceState.getInt(TABATA_CONFIG_TOTAL);

        update();
    }
}
