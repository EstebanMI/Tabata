package com.example.esteban.tabata;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.example.esteban.tabata.data.Programme;

import java.io.Serializable;

/**
 * Created by Esteban on 22/11/2017.
 */

public class DeleteDialogFragment extends DialogFragment {

    private static final String KEY_PROGRAMME = "programme";

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static DeleteDialogFragment newInstance(Programme prog) {
        DeleteDialogFragment f = new DeleteDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable(KEY_PROGRAMME, prog);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Programme mProg = getArguments().getParcelable(KEY_PROGRAMME);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("Suppression")
                .setMessage("Supprimer le programme '" + mProg.getNom() + "' ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDeleteDialogPositiveClick(DeleteDialogFragment.this, mProg);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public interface DeleteDialogListener {
        public void onDeleteDialogPositiveClick(DialogFragment dialog, Programme programme);
    }

    // Use this instance of the interface to deliver action events
    DeleteDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DeleteDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
