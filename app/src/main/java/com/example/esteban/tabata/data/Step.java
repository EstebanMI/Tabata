package com.example.esteban.tabata.data;

/**
 * Created by Esteban on 22/10/2017.
 */

public class Step {

    private String stepName;
    private int stepTime;

    public Step(String name, int time){
        setStepName(name);
        setStepTime(time);
    }

    // GETTER & SETTER
    public String getStepName() {
        return stepName;
    }

    private void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public int getStepTime() {
        return stepTime;
    }

    private void setStepTime(int stepTime) {
        this.stepTime = stepTime;
    }

    // GETTER convert time

    public int getMinutes() {
        int min = ((stepTime/1000)/60);
        return min;
    }

    public int getSecondes() {
        int secs = (int) (stepTime / 1000);
        return secs % 60;
    }

    public int getMillisecondes() {
        return (int) (stepTime % 1000);
    }
}
