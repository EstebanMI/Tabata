package com.example.esteban.tabata;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.esteban.tabata.data.Compteur;
import com.example.esteban.tabata.data.OnUpdateListener;
import com.example.esteban.tabata.data.Step;

import java.util.LinkedList;

public class TabataTimerActivity extends AppCompatActivity implements OnUpdateListener {

    // Constantes d'envoie
    public final static String TABATA_COMPTEUR = "compteur";

    // Data
    private int prepareTime;
    private int workTime;
    private int restTime;
    private int cycleNumber;
    private int tabataNumber;
    private int tabataRestTime;
    private int totalTime;

    // Elements graphiques
    private RelativeLayout mainLayout;
    private TextView partialTimerView;
    private TextView totalTimerView;
    private TextView stepView;
    private TextView cycleRestant;
    private TextView tabataRestant;
    private TextView nextStepInfo;
    private Button pauseButton;
    private Button resumeButton;

    private Compteur compteur;
    private LinkedList<Step> stepList;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabata_timer);

        // Récupération des données
        prepareTime = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_PREPARE, 0);
        workTime = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_WORK, 0);
        restTime = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_REST, 0);
        cycleNumber = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_CYCLE, 0);
        tabataNumber = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_TABATA, 0);
        tabataRestTime = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_TABATA_REST, 0);
        totalTime = getIntent().getIntExtra(TabataConfigurationActivity.TABATA_CONFIG_TOTAL, 0);

        // Récupération des buttons
        pauseButton = (Button) findViewById(R.id.btn_pause);
        resumeButton = (Button) findViewById(R.id.btn_resume);

        // Récupération des éléments graphiques
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        partialTimerView = (TextView) findViewById(R.id.txt_partialTimer);
        stepView = (TextView) findViewById(R.id.txt_step);
        totalTimerView = (TextView) findViewById(R.id.txt_totalTime);
        cycleRestant = (TextView) findViewById(R.id.cycleRestants);
        tabataRestant = (TextView) findViewById(R.id.tabataRestants);
        nextStepInfo = (TextView) findViewById(R.id.nextStep_info);

        start();
    }

    // Clic sur le bouton Stop
    public void onClickButtonStop(View view){
        cancel();
    }

    // Clic sur le bouton Pause
    public void onClickButtonPause(View view) {
        pause();
        resumeButton.setVisibility(View.VISIBLE);
        pauseButton.setVisibility(View.INVISIBLE);
    }

    // Clic sur le bouton Reprendre
    public void onClickButtonResume(View view){
        resume();
        pauseButton.setVisibility(View.VISIBLE);
        resumeButton.setVisibility(View.INVISIBLE);
    }

    // Créer un compteur et le démarre
    private void start(){
            setStepList();
            compteur = new Compteur(stepList);
            compteur.setCycleNumber(cycleNumber*tabataNumber);
            compteur.setTabataNumber(tabataNumber);
            compteur.setProgressBar(progressBar);
            compteur.addOnUpdateListener(this);
            compteur.start();
    }

    // Clic sur le bouton Pause
    private void pause(){
        compteur.pause();
    }

    // Clic sur le bouton Resume
    private void resume(){
        compteur.start();
    }

    // Clic sur le bouton Stop
    private void cancel(){
        compteur.pause();
        compteur = null;

        Intent intent = new Intent(this, TabataConfigurationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    // Construit une liste d'étape en fonction des données reçu
    private void setStepList(){
        stepList = new LinkedList<Step>();
        Step preparationStep = new Step("Préparation", prepareTime * 1000);
        Step workStep = new Step("Work", workTime * 1000);
        Step restStep = new Step("Repos", restTime * 1000);
        Step tabataRestStep = new Step("Repos entre les tabatas", tabataRestTime * 1000);

        if(preparationStep.getStepTime() > 0){
            stepList.addLast(preparationStep);
        }

        for (int j = 0; j < tabataNumber-1; j++){
            for (int i = 0; i < cycleNumber-1; i++){
                stepList.addLast(workStep);
                if(restStep.getStepTime() > 0){
                    stepList.addLast(restStep);
                }
            }
            stepList.addLast(workStep);
            if(tabataRestStep.getStepTime() > 0){
                stepList.addLast(tabataRestStep);
            }
        }
        for (int i = 0; i < cycleNumber-1; i++){
            stepList.addLast(workStep);
            if(restStep.getStepTime() > 0){
                stepList.addLast(restStep);
            }
        }
        stepList.addLast(workStep);
    }

    // Méthode qui met à jour l'affichage du compteur
    @Override
    public void onUpdate() {
        if(compteur != null){
            if(compteur.getCurrentStep() != null){
                // Mise à jour du compteur
                partialTimerView.setText("" + compteur.getMinutes() + ":"
                        + String.format("%02d", compteur.getSecondes()) + ":"
                        + String.format("%03d", compteur.getMillisecondes()));
                stepView.setText(compteur.getCurrentStep().getStepName());
                if(compteur.getNextStep() != null){
                    // Mise à jour de l'étape suivante
                    nextStepInfo.setText("" + compteur.getNextStep().getStepName() + " : " + compteur.getNextStep().getMinutes() + ":"
                            + String.format("%02d", compteur.getNextStep().getSecondes()) + ":"
                            + String.format("%03d", compteur.getNextStep().getMillisecondes()));
                }else{
                    nextStepInfo.setText("Finish!");
                }
                if(compteur.getCurrentStep().getStepName().equals("Préparation")){
                    mainLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreenDark));
                }else if(compteur.getCurrentStep().getStepName().equals("Work")){
                    mainLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMainDark));
                }else if(compteur.getCurrentStep().getStepName().equals("Repos") || compteur.getCurrentStep().getStepName().equals("Repos entre les tabatas")){
                    mainLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorOrangeDark));
                }
                tabataRestant.setText(String.valueOf(compteur.getTabataNumber()));
                cycleRestant.setText(String.valueOf(compteur.getCycleNumber()));

            }else{
                LinearLayout nextStep_layout = (LinearLayout) findViewById(R.id.nextStep_layout);
                nextStep_layout.setVisibility(View.INVISIBLE);
                compteur.getProgressBar().setProgress(0);
                partialTimerView.setText("Finish!");
                stepView.setText("");
                tabataRestant.setText("0");
                cycleRestant.setText("0");

                finish();
            }
        }else{
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(TABATA_COMPTEUR, compteur);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        compteur = savedInstanceState.getParcelable(TABATA_COMPTEUR);
    }


}
